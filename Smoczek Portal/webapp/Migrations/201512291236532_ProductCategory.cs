namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductCategory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StartNumber = c.Int(nullable: false),
                        EndNumber = c.Int(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateStoredProcedure(
                "dbo.ProductCategory_Insert",
                p => new
                    {
                        Name = p.String(),
                        StartNumber = p.Int(),
                        EndNumber = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[ProductCategories]([Name], [StartNumber], [EndNumber])
                      VALUES (@Name, @StartNumber, @EndNumber)
                      
                      DECLARE @ID int
                      SELECT @ID = [ID]
                      FROM [dbo].[ProductCategories]
                      WHERE @@ROWCOUNT > 0 AND [ID] = scope_identity()
                      
                      SELECT t0.[ID], t0.[Timestamp]
                      FROM [dbo].[ProductCategories] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            CreateStoredProcedure(
                "dbo.ProductCategory_Update",
                p => new
                    {
                        ID = p.Int(),
                        Name = p.String(),
                        StartNumber = p.Int(),
                        EndNumber = p.Int(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"UPDATE [dbo].[ProductCategories]
                      SET [Name] = @Name, [StartNumber] = @StartNumber, [EndNumber] = @EndNumber
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))
                      
                      SELECT t0.[Timestamp]
                      FROM [dbo].[ProductCategories] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            CreateStoredProcedure(
                "dbo.ProductCategory_Delete",
                p => new
                    {
                        ID = p.Int(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"DELETE [dbo].[ProductCategories]
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.ProductCategory_Delete");
            DropStoredProcedure("dbo.ProductCategory_Update");
            DropStoredProcedure("dbo.ProductCategory_Insert");
            DropTable("dbo.ProductCategories");
        }
    }
}
