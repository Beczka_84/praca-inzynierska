namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductsMigra2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "SaleDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "SaleDate", c => c.DateTime(nullable: false));
        }
    }
}
