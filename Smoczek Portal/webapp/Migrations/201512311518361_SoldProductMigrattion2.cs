namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SoldProductMigrattion2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SoldProducts", "SoldDate", c => c.DateTime(nullable: false));
            AlterStoredProcedure(
                "dbo.SoldProduct_Insert",
                p => new
                    {
                        ProductID = p.Int(),
                        Quantity = p.Int(),
                        SoldPrice = p.Double(),
                        SoldDate = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[SoldProducts]([ProductID], [Quantity], [SoldPrice], [SoldDate])
                      VALUES (@ProductID, @Quantity, @SoldPrice, @SoldDate)
                      
                      DECLARE @ID int
                      SELECT @ID = [ID]
                      FROM [dbo].[SoldProducts]
                      WHERE @@ROWCOUNT > 0 AND [ID] = scope_identity()
                      
                      SELECT t0.[ID], t0.[Timestamp]
                      FROM [dbo].[SoldProducts] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            AlterStoredProcedure(
                "dbo.SoldProduct_Update",
                p => new
                    {
                        ID = p.Int(),
                        ProductID = p.Int(),
                        Quantity = p.Int(),
                        SoldPrice = p.Double(),
                        SoldDate = p.DateTime(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"UPDATE [dbo].[SoldProducts]
                      SET [ProductID] = @ProductID, [Quantity] = @Quantity, [SoldPrice] = @SoldPrice, [SoldDate] = @SoldDate
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))
                      
                      SELECT t0.[Timestamp]
                      FROM [dbo].[SoldProducts] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
        }
        
        public override void Down()
        {
            DropColumn("dbo.SoldProducts", "SoldDate");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
