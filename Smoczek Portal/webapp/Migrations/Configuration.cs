namespace CloudCentre.Migrations
{
    using DAL.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CloudCentre.DAL.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CloudCentre.DAL.AppContext context)
        {
            var storeRole = new RoleStore<IdentityRole>(context);
            var managerRole = new RoleManager<IdentityRole>(storeRole);

            var role1 = new IdentityRole { Name = Roles.Admin };
            var role2 = new IdentityRole { Name = Roles.EndUser };

            managerRole.Create(role1);
            managerRole.Create(role2);


            var store = new UserStore<AppIdentityUser>(context);
            var manager = new UserManager<AppIdentityUser>(store);

            var passwordHash = new PasswordHasher();
            string password = passwordHash.HashPassword("Luk1326!!");

            var user = new AppIdentityUser()
            {
                UserName = "admin",
                Email = "lukasz_oz@interia.pl",
                PasswordHash = password,
                FirstName = "Lukasz",
                LastName = "Ozog",
                SecurityStamp = "a"
            };

            manager.Create(user);
            manager.AddToRole(user.Id, Roles.Admin);
          
        }
    }
}
