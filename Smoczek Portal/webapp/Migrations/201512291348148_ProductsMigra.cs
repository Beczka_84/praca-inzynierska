namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductsMigra : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CatalogNumber = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        OrgPrice = c.Double(nullable: false),
                        DiscountPrice = c.Double(nullable: false),
                        SaleDate = c.DateTime(nullable: false),
                        Comments = c.String(),
                        BrokerID = c.Int(nullable: false),
                        ProductCategoryID = c.Int(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Brokers", t => t.BrokerID, cascadeDelete: false)
                .ForeignKey("dbo.ProductCategories", t => t.ProductCategoryID, cascadeDelete: false)
                .Index(t => t.BrokerID)
                .Index(t => t.ProductCategoryID);
            
            CreateStoredProcedure(
                "dbo.Product_Insert",
                p => new
                    {
                        CatalogNumber = p.Int(),
                        Quantity = p.Int(),
                        OrgPrice = p.Double(),
                        DiscountPrice = p.Double(),
                        SaleDate = p.DateTime(),
                        Comments = p.String(),
                        BrokerID = p.Int(),
                        ProductCategoryID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Products]([CatalogNumber], [Quantity], [OrgPrice], [DiscountPrice], [SaleDate], [Comments], [BrokerID], [ProductCategoryID])
                      VALUES (@CatalogNumber, @Quantity, @OrgPrice, @DiscountPrice, @SaleDate, @Comments, @BrokerID, @ProductCategoryID)
                      
                      DECLARE @ID int
                      SELECT @ID = [ID]
                      FROM [dbo].[Products]
                      WHERE @@ROWCOUNT > 0 AND [ID] = scope_identity()
                      
                      SELECT t0.[ID], t0.[Timestamp]
                      FROM [dbo].[Products] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            CreateStoredProcedure(
                "dbo.Product_Update",
                p => new
                    {
                        ID = p.Int(),
                        CatalogNumber = p.Int(),
                        Quantity = p.Int(),
                        OrgPrice = p.Double(),
                        DiscountPrice = p.Double(),
                        SaleDate = p.DateTime(),
                        Comments = p.String(),
                        BrokerID = p.Int(),
                        ProductCategoryID = p.Int(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"UPDATE [dbo].[Products]
                      SET [CatalogNumber] = @CatalogNumber, [Quantity] = @Quantity, [OrgPrice] = @OrgPrice, [DiscountPrice] = @DiscountPrice, [SaleDate] = @SaleDate, [Comments] = @Comments, [BrokerID] = @BrokerID, [ProductCategoryID] = @ProductCategoryID
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))
                      
                      SELECT t0.[Timestamp]
                      FROM [dbo].[Products] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            CreateStoredProcedure(
                "dbo.Product_Delete",
                p => new
                    {
                        ID = p.Int(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"DELETE [dbo].[Products]
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Product_Delete");
            DropStoredProcedure("dbo.Product_Update");
            DropStoredProcedure("dbo.Product_Insert");
            DropForeignKey("dbo.Products", "ProductCategoryID", "dbo.ProductCategories");
            DropForeignKey("dbo.Products", "BrokerID", "dbo.Brokers");
            DropIndex("dbo.Products", new[] { "ProductCategoryID" });
            DropIndex("dbo.Products", new[] { "BrokerID" });
            DropTable("dbo.Products");
        }
    }
}
