namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ShopSet : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StoreSettings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateStoredProcedure(
                "dbo.StoreSettings_Insert",
                p => new
                    {
                        Name = p.String(),
                        Address = p.String(),
                    },
                body:
                    @"INSERT [dbo].[StoreSettings]([Name], [Address])
                      VALUES (@Name, @Address)
                      
                      DECLARE @ID int
                      SELECT @ID = [ID]
                      FROM [dbo].[StoreSettings]
                      WHERE @@ROWCOUNT > 0 AND [ID] = scope_identity()
                      
                      SELECT t0.[ID]
                      FROM [dbo].[StoreSettings] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            CreateStoredProcedure(
                "dbo.StoreSettings_Update",
                p => new
                    {
                        ID = p.Int(),
                        Name = p.String(),
                        Address = p.String(),
                    },
                body:
                    @"UPDATE [dbo].[StoreSettings]
                      SET [Name] = @Name, [Address] = @Address
                      WHERE ([ID] = @ID)"
            );
            
            CreateStoredProcedure(
                "dbo.StoreSettings_Delete",
                p => new
                    {
                        ID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[StoreSettings]
                      WHERE ([ID] = @ID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.StoreSettings_Delete");
            DropStoredProcedure("dbo.StoreSettings_Update");
            DropStoredProcedure("dbo.StoreSettings_Insert");
            DropTable("dbo.StoreSettings");
        }
    }
}
