namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BrokersDB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Brokers", "Contract", c => c.Int(nullable: false));
            AlterStoredProcedure(
                "dbo.Broker_Insert",
                p => new
                    {
                        FirstName = p.String(),
                        LastName = p.String(),
                        Card = p.String(),
                        Address = p.String(),
                        Contract = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Brokers]([FirstName], [LastName], [Card], [Address], [Contract])
                      VALUES (@FirstName, @LastName, @Card, @Address, @Contract)
                      
                      DECLARE @ID int
                      SELECT @ID = [ID]
                      FROM [dbo].[Brokers]
                      WHERE @@ROWCOUNT > 0 AND [ID] = scope_identity()
                      
                      SELECT t0.[ID], t0.[Timestamp]
                      FROM [dbo].[Brokers] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            AlterStoredProcedure(
                "dbo.Broker_Update",
                p => new
                    {
                        ID = p.Int(),
                        FirstName = p.String(),
                        LastName = p.String(),
                        Card = p.String(),
                        Address = p.String(),
                        Contract = p.Int(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"UPDATE [dbo].[Brokers]
                      SET [FirstName] = @FirstName, [LastName] = @LastName, [Card] = @Card, [Address] = @Address, [Contract] = @Contract
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))
                      
                      SELECT t0.[Timestamp]
                      FROM [dbo].[Brokers] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
        }
        
        public override void Down()
        {
            DropColumn("dbo.Brokers", "Contract");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
