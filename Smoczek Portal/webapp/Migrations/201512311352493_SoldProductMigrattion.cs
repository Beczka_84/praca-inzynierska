namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SoldProductMigrattion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SoldProducts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        SoldPrice = c.Double(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: false)
                .Index(t => t.ProductID);
            
            CreateStoredProcedure(
                "dbo.SoldProduct_Insert",
                p => new
                    {
                        ProductID = p.Int(),
                        Quantity = p.Int(),
                        SoldPrice = p.Double(),
                    },
                body:
                    @"INSERT [dbo].[SoldProducts]([ProductID], [Quantity], [SoldPrice])
                      VALUES (@ProductID, @Quantity, @SoldPrice)
                      
                      DECLARE @ID int
                      SELECT @ID = [ID]
                      FROM [dbo].[SoldProducts]
                      WHERE @@ROWCOUNT > 0 AND [ID] = scope_identity()
                      
                      SELECT t0.[ID], t0.[Timestamp]
                      FROM [dbo].[SoldProducts] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            CreateStoredProcedure(
                "dbo.SoldProduct_Update",
                p => new
                    {
                        ID = p.Int(),
                        ProductID = p.Int(),
                        Quantity = p.Int(),
                        SoldPrice = p.Double(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"UPDATE [dbo].[SoldProducts]
                      SET [ProductID] = @ProductID, [Quantity] = @Quantity, [SoldPrice] = @SoldPrice
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))
                      
                      SELECT t0.[Timestamp]
                      FROM [dbo].[SoldProducts] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            CreateStoredProcedure(
                "dbo.SoldProduct_Delete",
                p => new
                    {
                        ID = p.Int(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"DELETE [dbo].[SoldProducts]
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.SoldProduct_Delete");
            DropStoredProcedure("dbo.SoldProduct_Update");
            DropStoredProcedure("dbo.SoldProduct_Insert");
            DropForeignKey("dbo.SoldProducts", "ProductID", "dbo.Products");
            DropIndex("dbo.SoldProducts", new[] { "ProductID" });
            DropTable("dbo.SoldProducts");
        }
    }
}
