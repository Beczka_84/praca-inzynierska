namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BrokeEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Brokers", "Email", c => c.String());
            AddColumn("dbo.Brokers", "Phone", c => c.Int());
            AddColumn("dbo.Brokers", "TimeOfCreation", c => c.DateTime());
            AlterStoredProcedure(
                "dbo.Broker_Insert",
                p => new
                    {
                        FirstName = p.String(),
                        LastName = p.String(),
                        Card = p.String(),
                        Address = p.String(),
                        Contract = p.Int(),
                        Email = p.String(),
                        Phone = p.Int(),
                        TimeOfCreation = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[Brokers]([FirstName], [LastName], [Card], [Address], [Contract], [Email], [Phone], [TimeOfCreation])
                      VALUES (@FirstName, @LastName, @Card, @Address, @Contract, @Email, @Phone, @TimeOfCreation)
                      
                      DECLARE @ID int
                      SELECT @ID = [ID]
                      FROM [dbo].[Brokers]
                      WHERE @@ROWCOUNT > 0 AND [ID] = scope_identity()
                      
                      SELECT t0.[ID], t0.[Timestamp]
                      FROM [dbo].[Brokers] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
            AlterStoredProcedure(
                "dbo.Broker_Update",
                p => new
                    {
                        ID = p.Int(),
                        FirstName = p.String(),
                        LastName = p.String(),
                        Card = p.String(),
                        Address = p.String(),
                        Contract = p.Int(),
                        Email = p.String(),
                        Phone = p.Int(),
                        TimeOfCreation = p.DateTime(),
                        Timestamp_Original = p.Binary(maxLength: 8, fixedLength: true, storeType: "rowversion"),
                    },
                body:
                    @"UPDATE [dbo].[Brokers]
                      SET [FirstName] = @FirstName, [LastName] = @LastName, [Card] = @Card, [Address] = @Address, [Contract] = @Contract, [Email] = @Email, [Phone] = @Phone, [TimeOfCreation] = @TimeOfCreation
                      WHERE (([ID] = @ID) AND (([Timestamp] = @Timestamp_Original) OR ([Timestamp] IS NULL AND @Timestamp_Original IS NULL)))
                      
                      SELECT t0.[Timestamp]
                      FROM [dbo].[Brokers] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ID] = @ID"
            );
            
        }
        
        public override void Down()
        {
            DropColumn("dbo.Brokers", "TimeOfCreation");
            DropColumn("dbo.Brokers", "Phone");
            DropColumn("dbo.Brokers", "Email");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
