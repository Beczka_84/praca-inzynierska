﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using Hangfire;
using CloudCentre.Filters;
using Ninject.Web.Common;

[assembly: OwinStartup(typeof(CloudCentre.App_Start.Startup))]

namespace CloudCentre.App_Start
{
    public class Startup
    {
        internal static IDataProtectionProvider dataProtectionProvider { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            dataProtectionProvider = app.GetDataProtectionProvider();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                CookieName = "Smoczek",
                ExpireTimeSpan = System.TimeSpan.FromMinutes(60)
            });

            GlobalConfiguration.Configuration.UseSqlServerStorage("AppContext").UseNinjectActivator(new Bootstrapper().Kernel);

            app.UseHangfireDashboard("/hangfire", new DashboardOptions { AuthorizationFilters = new[] { new RestrictiveAuthorizationFilter() } });
            app.UseHangfireServer();
        }
    }
}
