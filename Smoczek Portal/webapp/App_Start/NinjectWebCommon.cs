[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CloudCentre.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CloudCentre.App_Start.NinjectWebCommon), "Stop")]

namespace CloudCentre.App_Start
{
    using System;
    using System.Web;
    using Ninject.Extensions.Conventions;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using DAL;
    using Services.Concrete;
    using Microsoft.AspNet.Identity;
    using DAL.Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.Owin.Security;
    using Hangfire;
    using NLog;
    using Services.Abstract;
    using Filters;
    using Ninject.Web.Mvc.FilterBindingSyntax;
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<AppContext>().ToSelf().InRequestScope();

            kernel.Bind(x => { x.From(typeof(UserManagerService).Assembly).SelectAllClasses().EndingWith("Service").BindDefaultInterface(); });


            kernel.BindFilter<ProductsCategoryActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<ProductsCategoryAttribute>();
            kernel.BindFilter<BrokerActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<BrokerAttribute>();


            kernel.Bind(typeof(IUserStore<AppIdentityUser>)).To(typeof(UserStore<AppIdentityUser>)).WithConstructorArgument("context", kernel.Get<AppContext>());
            kernel.Bind(typeof(UserManager<AppIdentityUser>)).To(typeof(UserManager<AppIdentityUser>)).WithConstructorArgument("store", kernel.Get<IUserStore<AppIdentityUser>>());

            kernel.Bind(typeof(IRoleStore<IdentityRole, string>)).To(typeof(RoleStore<IdentityRole>)).WithConstructorArgument("context", kernel.Get<AppContext>());
            kernel.Bind(typeof(RoleManager<IdentityRole>)).To(typeof(RoleManager<IdentityRole>)).WithConstructorArgument("store", kernel.Get<IRoleStore<IdentityRole, string>>());

            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication);
            kernel.Bind<ILogger>().ToMethod(x => LogManager.GetCurrentClassLogger());
            kernel.Bind<IBackgroundJobClient>().To<BackgroundJobClient>();
        }        
    }
}
