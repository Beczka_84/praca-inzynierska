﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.Models
{
    public class ProductCategory
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StartNumber { get; set; }
        public int EndNumber { get; set; }

        public virtual ICollection<Product> Products { get; set; }


        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}