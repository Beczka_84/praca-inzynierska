﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.Models
{
    public class SoldProduct
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public double SoldPrice { get; set; }

        public DateTime SoldDate { get; set; }

        public virtual Product Product { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}