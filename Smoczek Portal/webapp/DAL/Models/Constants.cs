﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.Models
{
    public static class Constants
    {
        public const string AppUser = "AppUser";
        public const string RolesViewBag = "RolesViewBag";
        public const string ProductCategoryViewBag = "ProductCategoryViewBag";
        public const string ProductgViewBag = "ProductViewBag";
        public const string BrokerViewBag = "BrokerViewBag";

    }
}