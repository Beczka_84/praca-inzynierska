﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.Models
{
    public class Product
    {
        public int ID { get; set; }
        public int CatalogNumber { get; set; }

        public int Quantity { get; set; }
        public double OrgPrice { get; set; }
        public double DiscountPrice { get; set; }
        public DateTime? SaleDate { get; set; }
        public string Comments { get; set; }
        public int BrokerID { get; set; }
        public int ProductCategoryID { get; set; }


        [Timestamp]
        public byte[] Timestamp { get; set; }


        public virtual Broker Broker { get; set; }
        public virtual ICollection<SoldProduct> SoldProducts { get; set; }

        public virtual ProductCategory ProductCategory { get; set; }

        
    }
}