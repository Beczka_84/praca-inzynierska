﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.Models
{
    public class Broker
    {
        public int ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Card { get; set; }
        public string Address { get; set; }
        public int Contract { get; set; }

        public string Email { get; set; }
        public int? Phone { get; set; }

        public DateTime? TimeOfCreation { get; set;}

        public virtual ICollection<Product> Products { get; set; }


        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}