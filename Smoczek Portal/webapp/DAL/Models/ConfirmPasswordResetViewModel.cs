﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.Models
{
    public class ConfirmPasswordResetViewModel
    {
        [Required(ErrorMessage ="Token musi zostać podany")]
        public string Token { get; set; }

        [Required]
        public string Id { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
    }
}