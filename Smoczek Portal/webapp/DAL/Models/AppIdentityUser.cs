﻿using Microsoft.AspNet.Identity.EntityFramework;


namespace CloudCentre.DAL.Models
{
    public class AppIdentityUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}