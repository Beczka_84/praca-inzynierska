﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.Models
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string EndUser = "End User";
    }
}