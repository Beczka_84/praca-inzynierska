﻿using CloudCentre.DAL.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL
{
    public class AppContext : IdentityDbContext<IdentityUser>
    {
        private readonly ILogger _logger;

        public AppContext() : base("AppContext")
        {

        }

        public AppContext(ILogger logger) : base("AppContext")
        {
            this._logger = logger;
            Database.Log = x => logger.Trace(x);
        }

        public virtual DbSet<Broker> Brokers { get; set; }
        public virtual DbSet<StoreSettings> StoreSettings { get; set; }
        public virtual DbSet<ProductCategory> ProductCategorys { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<SoldProduct> SoldProducts { get; set; }





        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Broker>().MapToStoredProcedures();
            modelBuilder.Entity<StoreSettings>().MapToStoredProcedures();
            modelBuilder.Entity<ProductCategory>().MapToStoredProcedures();
            modelBuilder.Entity<Product>().MapToStoredProcedures();
            modelBuilder.Entity<SoldProduct>().MapToStoredProcedures();


            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityUser>().ToTable("Users");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
        }
    }
}