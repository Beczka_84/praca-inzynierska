﻿#region Using

using CloudCentre.Controllers;
using CloudCentre.Filters;
using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    [CurrentUserActionFilter]
    public class HomeController : BaseController
    {
        // GET: home/index
        public ActionResult Index()
        {
            return View();
        }

       
    }
}