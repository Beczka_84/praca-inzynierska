﻿#region Using

using AutoMapper;
using CloudCentre.Controllers;
using CloudCentre.DAL.Models;
using CloudCentre.DAL.ViewModels;
using CloudCentre.Filters;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services.Abstract;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    [Authorize(Roles = Roles.Admin)]
    [CurrentUserActionFilter]
    [RolesActionFilter]
    public class SettingsController : BaseController
    {
        private readonly IStoreSettingsService _storeSettingsService;
        private readonly IUserManagerService _userManagerService;
        private readonly IProductCategoryService _productCategoryService;


        public SettingsController(IStoreSettingsService storeSettingsService, IUserManagerService userManagerService, IProductCategoryService productCategoryService)
        {
            this._storeSettingsService = storeSettingsService;
            this._userManagerService = userManagerService;
            this._productCategoryService = productCategoryService;
        }

        [HttpGet]
        public ActionResult ShopSettings()
        {
            StoreSettingsViewModel viewModel = Mapper.Map<StoreSettingsViewModel>(_storeSettingsService.GetStoreSettings());
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult CreateShopSettings()
        {
            return View(new StoreSettingsViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateShopSettings(StoreSettingsViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            StoreSettings storeSettings = Mapper.Map<StoreSettings>(viewModel);
            bool result = _storeSettingsService.CreateStoreSettings(storeSettings);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy dodawaniu ustawień - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Dodano ustawienia sklepu";
            return RedirectToAction("ShopSettings");
        }

        [HttpGet]
        public ActionResult EditShopSettings(int Id)
        {
            if (Id == 0) {
                return RedirectToAction("ShopSettings");
            }

            StoreSettings storeSettings = _storeSettingsService.GetStoreSettings(Id);
            if(storeSettings == null)
            {
                TempData["Error"] = "Nie ma takich ustawień";
                return RedirectToAction("ShopSettings");
            }

            StoreSettingsViewModel viewModel = Mapper.Map<StoreSettingsViewModel>(storeSettings);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditShopSettings(StoreSettingsViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            StoreSettings storeSettings = Mapper.Map<StoreSettings>(viewModel);
            bool result = _storeSettingsService.EditStoreSettings(storeSettings);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy edytowaniu ustawień - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Ustawienia sklepu zostały edytowane pomyślnie";
            return RedirectToAction("ShopSettings");
        }

        [HttpGet]
        public ActionResult ConfirmDeleteShopSettings(int Id)
        {
            StoreSettings storeSettings = _storeSettingsService.GetStoreSettings(Id);
            if (storeSettings == null)
            {
                TempData["Error"] = "Nie ma takich ustawień";
                return RedirectToAction("ShopSettings");
            }

            StoreSettingsViewModel viewModel = Mapper.Map<StoreSettingsViewModel>(storeSettings);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDeleteShopSettings(StoreSettingsViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            StoreSettings storeSettings = _storeSettingsService.GetStoreSettings(viewModel.ID);

            if(storeSettings == null)
            {
                TempData["Error"] = "Nie ma takich ustawień";
                return RedirectToAction("ShopSettings");
            }
         
            bool result = _storeSettingsService.DeleteStoreSettings(storeSettings);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy usuwaniu ustawień - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Ustawienia sklepu zostały usunięte pomyślnie";
            return RedirectToAction("ShopSettings");
        }


        [HttpGet]
        public ActionResult AppUsersList()
        {
            List<AppIdentityUserViewModel> viewModelList = Mapper.Map<List<AppIdentityUserViewModel>>(_userManagerService.GetAllUsers());
            return View(viewModelList);
        }


        [HttpGet]
        public async Task<ActionResult> ChangeAppUserRole(string Id)
        {
            if (Id == null)
            {
                return RedirectToAction("ShopSettings");
            }
            AppIdentityUser appIdentityUser = await _userManagerService.GetUserById(Id);

            if(appIdentityUser == null)
            {
                TempData["Error"] = "Nie ma takiego użytkownika";
                return RedirectToAction("AppUsersList");
            }

            AppIdentityUserViewModel viewModel = Mapper.Map<AppIdentityUserViewModel>(appIdentityUser);
            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> ChangeAppUserRole(AppIdentityUserViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            AppIdentityUser appIdentityUser = await _userManagerService.GetUserById(viewModel.Id);

            if(appIdentityUser == null)
            {
                TempData["Error"] = "Nie ma takiego użytkownika";
                return RedirectToAction("AppUsersList");
            }

            string role = _userManagerService.GetUserRole(appIdentityUser.Id).FirstOrDefault();
            string roleToChange = _userManagerService.GetRole(viewModel.RoleId).Name;

            if (role == null || roleToChange == null)
            {
                TempData["Error"] = "Użytkownik nie ma przypisanej roli";
                return RedirectToAction("AppUsersList");
            }

            IdentityResult removeFromRoleResult =  await _userManagerService.RemoveUserFromRoleAsync(appIdentityUser, role);
            IdentityResult addToRoleResult = await _userManagerService.AddUserToRoleAsync(appIdentityUser, roleToChange);

            TempData["message"] = "Rola została pomyslnie zmieniona dla uzytkownika";
            return RedirectToAction("AppUsersList");
        }


        [HttpGet]
        public async Task<ActionResult> RemoveAppUserRole(string Id)
        {
            if (Id == null)
            {
                return RedirectToAction("ShopSettings");
            }
            AppIdentityUser appIdentityUser = await _userManagerService.GetUserById(Id);

            if (appIdentityUser == null)
            {
                TempData["Error"] = "Nie ma takiego użytkownika";
                return RedirectToAction("AppUsersList");
            }

            AppIdentityUserViewModel viewModel = Mapper.Map<AppIdentityUserViewModel>(appIdentityUser);
            return View(viewModel);
        }


        [HttpPost]
        public async Task<ActionResult> RemoveAppUserRole(AppIdentityUserViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            AppIdentityUser appIdentityUser = await _userManagerService.GetUserById(viewModel.Id);

            if (appIdentityUser == null)
            {
                TempData["Error"] = "Nie ma takiego użytkownika";
                return RedirectToAction("AppUsersList");
            }

            IdentityResult removeUser = await _userManagerService.RemoveUserData(appIdentityUser);

            if (!removeUser.Succeeded)
            {
                TempData["Error"] = "Wystapił błąd podczas usuwania uzytkownika";
                return RedirectToAction("AppUsersList");
            }

            TempData["message"] = "Użytkownik został usuniety poprawnie";
            return RedirectToAction("AppUsersList");
        }

        [HttpGet]
        public async Task<ActionResult> EditAppUsers(string Id)
        {
            if (Id == null)
            {
                return RedirectToAction("ShopSettings");
            }

            AppIdentityUser appIdentityUser = await _userManagerService.GetUserById(Id);

            if (appIdentityUser == null)
            {
                TempData["Error"] = "Nie ma takiego użytkownika";
                return RedirectToAction("AppUsersList");
            }

            IdentityUserEditViewModel viewModel = Mapper.Map<IdentityUserEditViewModel>(appIdentityUser);
            return View(viewModel);
        }


        [HttpPost]
        public async Task<ActionResult> EditAppUsers(IdentityUserEditViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            AppIdentityUser appIdentityUser = await _userManagerService.GetUserById(viewModel.Id);

         

            if (appIdentityUser == null)
            {
                TempData["Error"] = "Nie ma takiego użytkownika";
                return RedirectToAction("AppUsersList");
            }

            string OrgEmail = appIdentityUser.Email;

            Mapper.Map(viewModel, appIdentityUser);
            IdentityResult result = await _userManagerService.UpdateUserData(appIdentityUser);

            if (!result.Succeeded)
            {
                TempData["Error"] = "Wystapił błąd podczas edytowany uzytkownika";
                return RedirectToAction("AppUsersList");
            }

            if (OrgEmail != viewModel.Email)
            {
                string token = await _userManagerService.GenerateEmailToken(appIdentityUser.Id);
                await _userManagerService.SendConfirmationEMail(Url.Action("ConfirmEmail", "Account", new { userId = appIdentityUser.Id, code = token, action = "ShopSettings", controller = "Settings" }, protocol: Request.Url.Scheme), appIdentityUser.Id);
                appIdentityUser.EmailConfirmed = false;
                TempData["message"] = "Email potwierdzający konto został wysłany";
            }

            if (TempData["message"] != null)
            {
                TempData["message"] = string.Join(" ", TempData["message"], "oraz użytkownik został edytowany poprawnie");
            }
            else
            {
                TempData["message"] = "Użytkownik został edytowany poprawnie";
            }

            Session[CloudCentre.DAL.Models.Constants.AppUser] = appIdentityUser;

            return RedirectToAction("AppUsersList");
        }


        [HttpGet]
        public ActionResult ProductCategorys()
        {
            List<ProductCategoryViewModel> viewModel = Mapper.Map<List<ProductCategoryViewModel>>(_productCategoryService.GetProductCategorys());
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult CreateProductCategory()
        {
            
            return View(new ProductCategoryViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProductCategory(ProductCategoryViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            if (_productCategoryService.CheckIfProductCategoryisTaken(viewModel.Name))
            {
                ModelState.AddModelError("", "Ta nazwa kategori jest już zajeta");
                return View(viewModel);
            }


            ProductCategory productCategory = Mapper.Map<ProductCategory>(viewModel);
            bool result = _productCategoryService.CreateProductCategory(productCategory);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy dodawaniu kategori produktu - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Dodano kategorie produktu";
            return RedirectToAction("ProductCategorys");
        }


        [HttpGet]
        public ActionResult EditProductCategory(int Id)
        {
            if (Id == 0)
            {
                return RedirectToAction("CreateProductCategory");
            }

            ProductCategory productCategory = _productCategoryService.GetProductCategory(Id);
            if (productCategory == null)
            {
                TempData["Error"] = "Nie ma takiej kotegori";
                return RedirectToAction("ProductCategorys");
            }

            ProductCategoryViewModel viewModel = Mapper.Map<ProductCategoryViewModel>(productCategory);
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProductCategory(ProductCategoryViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            ProductCategory productCategory = _productCategoryService.GetProductCategory(viewModel.ID);

            if (productCategory == null)
            {
                TempData["Error"] = "Nie ma takiej kotegori";
                return RedirectToAction("ProductCategorys");
            }

            if (_productCategoryService.CheckIfProductCategoryisTaken(viewModel.Name))
            {
                if (productCategory.ID != viewModel.ID)
                {
                    ModelState.AddModelError("", "Ta nazwa kategori jest już zajeta");
                    return View(viewModel);
                }
            }


            Mapper.Map(viewModel, productCategory);

            bool result = _productCategoryService.EditProductCategory(productCategory);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy edytowaniu kategori - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Dane kategori zstały eytowane pomyślnie";
            return RedirectToAction("ProductCategorys");
        }

        [HttpGet]
        public ActionResult ConfirmDeleteProductCategory(int Id)
        {
            ProductCategory productCategory = _productCategoryService.GetProductCategory(Id);
            if (productCategory == null)
            {
                TempData["Error"] = "Nie ma takiej kategori";
                return RedirectToAction("ProductCategorys");
            }

            ProductCategoryViewModel viewModel = Mapper.Map<ProductCategoryViewModel>(productCategory);
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDeleteProductCategory(ProductCategoryViewModel viewModel)
        {
            ProductCategory productCategory = _productCategoryService.GetProductCategory(viewModel.ID);

            if (productCategory == null)
            {
                TempData["Error"] = "Nie ma takiej kategori";
                return RedirectToAction("ProductCategorys");
            }

            bool result = _productCategoryService.DeleteProductCategory(productCategory);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy usuwaniu kategori - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "kategoria została usunieta pomyślnie";
            return RedirectToAction("ProductCategorys");
        }



    }



}