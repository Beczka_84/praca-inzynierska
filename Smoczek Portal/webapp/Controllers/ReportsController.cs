﻿#region Using

using CloudCentre.Controllers;
using CloudCentre.DAL.Models;
using CloudCentre.Filters;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    [CurrentUserActionFilter]
    [Broker]
    public class ReportsController : BaseController
    {
        private readonly IProductService _productService;
        private readonly IBrokerService _brokerService;


        public ReportsController(IProductService productService, IBrokerService brokerService)
        {
            this._productService = productService;
            this._brokerService = brokerService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ShowReport(ReportViewModel viewModel)
        {
           var productList =  _productService.GetProducts().Where(x => x.BrokerID == viewModel.BrookerId).Select(y => y.ID).ToArray();
            ReportViewModel reportViewModel = new ReportViewModel();
            reportViewModel.SoldProducts = _productService.GetSoldProducts().Where(x => productList.Contains(x.ProductID)).ToList();

            Broker broker = _brokerService.GetBroker(viewModel.BrookerId);

            if(broker == null)
            {
                TempData["Error"] = "Nie ma takiego komisanta";
                return RedirectToAction("Index", " Reports");
            }
            reportViewModel.FullName = string.Join(" ", broker.FirstName, broker.LastName);
            reportViewModel.ConctractNumber = broker.Contract.ToString();
            foreach ( var item in reportViewModel.SoldProducts)
            {
                reportViewModel.TotalValue += item.SoldPrice * item.Quantity;
            }

            var groupedList = reportViewModel.SoldProducts.GroupBy(x => GetFirstDayInMonth(x.SoldDate)).Select(x => new
            {
                Value = x.Sum(y => y.SoldPrice),
                Date = x.Key
            }).ToList();

            List<double[]> ChartData = new List<double[]>();

            foreach (var item in groupedList)
            {
                List<double> arr = new List<double>();
                arr.Add(item.Date.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);
                arr.Add(item.Value);
                ChartData.Add(arr.ToArray());
            }
            string json = JsonConvert.SerializeObject(ChartData);
            reportViewModel.ChartData = json;
            return View("_ShowReport", reportViewModel);
        }


        private DateTime GetFirstDayInMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Date.Year, dateTime.Date.Month, dateTime.Date.Day);
        }


    }
}