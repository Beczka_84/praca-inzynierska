﻿using AutoMapper;
using CloudCentre.DAL.Models;
using CloudCentre.Filters;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    [ProductsCategory]
    [CurrentUserActionFilter]
    public class ProductsController : BaseController
    {
        private readonly IProductService _productService;
        private readonly IProductCategoryService _productCategoryService;
        private readonly IBrokerService _brokerService;
        public ProductsController(IProductService productService, IProductCategoryService productCategoryService, IBrokerService brokerService)
        {
            this._productService = productService;
            this._brokerService = brokerService;
            this._productCategoryService = productCategoryService;
        }



        [HttpGet]
        public ActionResult ShowAllProducts()
        {
            ProductViewModelList viewModel = new ProductViewModelList();
            viewModel.ViewModel = Mapper.Map<List<ProductViewModel>>(_productService.GetProducts().Where(x=>x.SaleDate == null));
            return View(viewModel);
        }


        [HttpGet]
        public ActionResult Products(int brokerId)
        {
            if (brokerId == 0)
            {
                TempData["Error"] = "Nie ma takiego komisanta";
                return RedirectToAction("Brokers", "Broker");
            }
            ProductViewModelList viewModel = new ProductViewModelList();
            viewModel.ViewModel = Mapper.Map<List<ProductViewModel>>(_productService.GetProducts(brokerId));
            viewModel.BrokerId = brokerId;
            var broker = _brokerService.GetBroker(brokerId);
            viewModel.BrokerName = string.Join(" ", broker?.FirstName ?? string.Empty, broker?.LastName ?? string.Empty);
            viewModel.Contract = broker?.Contract ?? 0;
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult RetriveLastProductNumber(int brokerId, int productCategoryId)
        {
            return Json(_productService.RetriveLastProductNumber(brokerId, productCategoryId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateProduct(int Id)
        {
            if (Id == 0)
            {
                TempData["Error"] = "Nie ma takiego komisanta";
                return RedirectToAction("Brokers", "Broker");
            }

            ProductViewModel viewModel =    new ProductViewModel() { BrokerID = Id };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProduct(ProductViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            if (_productService.CheckIfProductNumberisTaken(viewModel.BrokerID, viewModel.CatalogNumber))
            {
                ModelState.AddModelError("", "Ten numer produktu jest już zajety");
                return View(viewModel);
            }

            if(viewModel.CatalogNumber > (_productCategoryService.GetProductCategory(viewModel.ProductCategoryID)?.EndNumber ?? 0))
            {
                ModelState.AddModelError("", "Numer katalogowy wiekszy niż zapisano w katalogu danego produktu");
                return View(viewModel);
            }

            Product product = Mapper.Map<Product>(viewModel);
            bool result = _productService.CreateProduct(product);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy dodawaniu produktu - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Dodano produkt";
            return RedirectToAction("Products", new { brokerId = viewModel.BrokerID});
        }


        [HttpGet]
        public ActionResult EditProduct(int Id)
        {
            if (Id == 0)
            {
                return RedirectToAction("Brokers", "Broker");
            }

            Product product = _productService.GetProduct(Id);
            if (product == null)
            {
                TempData["Error"] = "Nie ma takiego produktu";
                return RedirectToAction("Brokers", "Broker");
            }

            ProductViewModel viewModel = Mapper.Map<ProductViewModel>(product);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProduct(ProductViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            Product product = _productService.GetProduct(viewModel.ID);

            if (product == null)
            {
                TempData["Error"] = "Nie ma takiego produktu";
                return RedirectToAction("Products", new { brokerId = viewModel.BrokerID });
            }

            if (_productService.CheckIfProductNumberisTaken(viewModel.BrokerID, viewModel.CatalogNumber))
            {
                if (product.ID != viewModel.ID)
                {
                    ModelState.AddModelError("", "Ten numer produktu jest już zajety");
                    return View(viewModel);
                }
            }

            if (viewModel.CatalogNumber > (_productCategoryService.GetProductCategory(viewModel.ProductCategoryID)?.EndNumber ?? 0))
            {
                ModelState.AddModelError("", "Numer katalogowy wiekszy niż zapisano w katalogu danego produktu");
                return View(viewModel);
            }

            Mapper.Map(viewModel, product);

            bool result = _productService.EditProduct(product);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy edytowaniu produktu - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Produkt został edytowany pomyślnie";
            return RedirectToAction("Products", new { brokerId = viewModel.BrokerID });
        }

        [HttpGet]
        public ActionResult ConfirmDeleteProduct(int Id)
        {
            Product product = _productService.GetProduct(Id);
            if (product == null)
            {
                TempData["Error"] = "Nie ma takiego produktu";
                return RedirectToAction("Brokers", "Broker");
            }

            ProductViewModel viewModel = Mapper.Map<ProductViewModel>(product);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDeleteProduct(ProductViewModel viewModel)
        {
            Product product = _productService.GetProduct(viewModel.ID);

            if (product == null)
            {
                TempData["Error"] = "Nie ma takiego produktu";
                return RedirectToAction("Brokers", "Broker");
            }

            bool result = _productService.DeleteProduct(product);

            if (!result)
            {
                ModelState.Clear();
                ModelState.AddModelError("", "Wystąpił błąd przy usuwaniu produktu - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "produkt został usuniety pomyślnie";
            return RedirectToAction("Products", new { brokerId = viewModel.BrokerID });
        }

        [HttpGet]
        public ActionResult SellProduct(int Id)
        {
            if (Id == 0)
            {
                return RedirectToAction("Brokers", "Broker");
            }

            Product product = _productService.GetProduct(Id);

            if (product == null)
            {
                TempData["Error"] = "Nie ma takiego produktu";
                return RedirectToAction("Brokers", "Broker");
            }

            ProductViewModel viewModel = Mapper.Map<ProductViewModel>(product);
            int soldProducts = _productService.GetSoldProducts(Id).Count();
            viewModel.Quantity = viewModel.Quantity - soldProducts;
            return View(viewModel);
        }

       
        public ActionResult SellProductWithPrice(int Id, bool orgPrice)
        {
            if (Id == 0 )
            {
                return RedirectToAction("Brokers", "Broker");
            }

            Product product = _productService.GetProduct(Id);

            if (product == null)
            {
                TempData["Error"] = "Nie ma takiego produktu";
                return RedirectToAction("Brokers", "Broker");
            }

            SoldProduct soldProduct = Mapper.Map<SoldProduct>(product);
            if (orgPrice)
            {
                if(product.OrgPrice == 0)
                {
                    TempData["Error"] = "Produkt ma orginalną cenę 0 zł";
                    return RedirectToAction("Products", new { brokerId = product.BrokerID });
                }
                soldProduct.SoldPrice = product.OrgPrice;
            }
            else
            {
                if (product.DiscountPrice == 0)
                {
                    TempData["Error"] = "Produkt jest przecenionony na 0 zł";
                    return RedirectToAction("Products", new { brokerId = product.BrokerID });
                }
                soldProduct.SoldPrice = product.DiscountPrice;
            }
          
            bool result = _productService.SaleProduct(soldProduct);

            if (!result)
            {
                TempData["Error"] = "Wystąpił błąd przy sprzedawaniu produktu - sprawdż logi";
                return RedirectToAction("Products", new { brokerId = product.BrokerID });
            }


           if(product.Quantity == _productService.GetSoldProducts(Id).Count())
            {
                product.SaleDate = DateTime.Now;
                bool result2 = _productService.EditProduct(product);

                if (!result2)
                {
                    TempData["Error"] = "Wystąpił błąd przy edytowaniu daty sprzedaży produktu - sprawdż logi";
                    return RedirectToAction("Products", new { brokerId = product.BrokerID });
                }
            }

            TempData["message"] = "Sprzedano produkt pomyślnie";
            return RedirectToAction("SellProduct", new { Id = product.ID });
        }


        [HttpGet]
        public ActionResult ShowSoldProducts(int Id)
        {
            if (Id == 0)
            {
                return RedirectToAction("Brokers", "Broker");
            }
            SoldProductsViewModelList soldProductsViewModelList = new SoldProductsViewModelList();
            soldProductsViewModelList.viewModel = Mapper.Map<List<SoldProductsViewModel>>(_productService.GetSoldProducts(Id).ToList());
            soldProductsViewModelList.brokerID = _productService.GetProduct(Id)?.BrokerID ?? 0;
            return View(soldProductsViewModelList);
        }


        [HttpGet]
        public ActionResult ChangeSoldProducts(int Id)
        {
            if (Id == 0)
            {
                return RedirectToAction("Brokers", "Broker");
            }

            SoldProduct soldProduct = _productService.GetSoldProduct(Id);
            SoldProductsViewModel soldProductsViewModelList = Mapper.Map<SoldProductsViewModel>(soldProduct);
            return View(soldProductsViewModelList);
        }


        [HttpPost]
        public ActionResult ChangeSoldProducts(SoldProductsViewModel viewModel)
        {
            if (viewModel.ID == 0 && viewModel.SoldDate == null)
            {
                return RedirectToAction("Brokers", "Broker");
            }

            SoldProduct soldProduct = _productService.GetSoldProduct(viewModel.ID);
            soldProduct.SoldDate = viewModel.SoldDate;
            var result = _productService.EditSaleProduct(soldProduct);

            if (!result)
            {
                TempData["Error"] = "Wystąpił błąd przy edytowaniu daty sprzedaży produktu - sprawdż logi";
                return RedirectToAction("ShowSoldProducts", new { Id = viewModel.ProductID });
            }

            TempData["message"] = "Sprzedany produkt edytowano pomyślnie";
            return RedirectToAction("ShowSoldProducts", new { Id = viewModel.ProductID });
       }

        public ActionResult RemoveSoldProducts(int Id)
        {
            if (Id == 0)
            {
                return RedirectToAction("Brokers", "Broker");
            }

            SoldProduct soldProduct = _productService.GetSoldProduct(Id);

            if (soldProduct == null)
            {
                TempData["Error"] = "Nie ma takiego sprzedanego produktu";
                return RedirectToAction("Brokers", "Broker");
            }

            bool result = _productService.RemoveSoldProduct(soldProduct);

            if (!result)
            {
                TempData["Error"] = "Wystąpił błąd przy usuwania sprzedanego produktu - sprawdż logi";
                return RedirectToAction("ShowSoldProducts", new { Id = soldProduct.ProductID });
            }

            Product product = _productService.GetProduct(soldProduct.ProductID);

            if (product == null)
            {
                TempData["Error"] = "Nie ma takiego produktu";
                return RedirectToAction("Brokers", "Broker");
            }

            if (product.SaleDate != null)
            {
                product.SaleDate = null;
                bool result2 = _productService.EditProduct(product);

                if (!result2)
                {
                    TempData["Error"] = "Wystąpił błąd przy edytowaniu daty sprzedaży produktu - sprawdż logi";
                    return RedirectToAction("ShowSoldProducts", new { Id = soldProduct.ProductID });
                }
            }

            TempData["message"] = "Sprzedany produkt usunieto pomyślnie";
            return RedirectToAction("ShowSoldProducts", new { Id = soldProduct.ProductID });
        }

    }
}