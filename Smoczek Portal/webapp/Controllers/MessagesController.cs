﻿using AutoMapper;
using CloudCentre.Infrastructure.Sms;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services.Abstract;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    public class MessagesController : BaseController
    {
        private readonly IBrokerService _brokerService;
        private readonly IBackgroundJobClient _hangFireService;


        public MessagesController(IBrokerService brokerService, IBackgroundJobClient hangFireService)
        {
            this._brokerService = brokerService;
            this._hangFireService = hangFireService;
        }

        [HttpGet]
        public ActionResult SmsMessages()
        {
            var list = Mapper.Map<List<BrokerViewModel>>(_brokerService.GetBrokers().Where(x=>x.Phone!= null));
            SmsViewModel viewModel = new SmsViewModel();
            viewModel.BrokerSelect = new SelectList(list, "ID", "FullName");
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SmsMessages(SmsViewModel viewModel)
        {
            if(viewModel.BrokersIds != null && viewModel.BrokersIds.Length > 0) {
                foreach (var item in viewModel.BrokersIds)
                {
                    var broker = _brokerService.GetBroker(item);
                    _hangFireService.Enqueue<SmsProvider>(x => x.Send(broker.Phone.ToString(), viewModel.Message));
                }    
            }
            TempData["message"] = "Wysłano Sms-y";
            return RedirectToAction("Brokers", "Broker");
            }
        }
    }