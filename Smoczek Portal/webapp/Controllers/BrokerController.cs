﻿using AutoMapper;
using CloudCentre.DAL.Models;
using CloudCentre.Filters;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    [CurrentUserActionFilter]
    public class BrokerController : BaseController
    {
        private readonly IBrokerService _brokerService;
        public BrokerController(IBrokerService brokerService)
        {
            this._brokerService = brokerService;
        }

        [HttpGet]
        public ActionResult Brokers()
        {
            List<BrokerViewModel> viewModel = Mapper.Map<List<BrokerViewModel>>(_brokerService.GetBrokers());
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult CreateBroker()
        {
           BrokerViewModel viewModel = new BrokerViewModel() { Contract = _brokerService.RetriveLastContranctNumber() };
           return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateBroker(BrokerViewModel viewModel)
        {
            if (!ModelState.IsValid) {
                return View(viewModel);
            }

            if (_brokerService.CheckIfContractNumberisTaken(viewModel.Contract))
            {
                ModelState.AddModelError("", "Ten numer umowy jest już zajety");
                return View(viewModel);
            }


            Broker broker = Mapper.Map<Broker>(viewModel);
            bool result = _brokerService.CreateBroker(broker);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy dodawaniu komisanta - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Dodano komisanta";
            return RedirectToAction("Brokers");
        }


        [HttpGet]
        public ActionResult EditBroker(int Id)
        {
            if (Id == 0)
            {
                return RedirectToAction("Brokers");
            }

            Broker broker = _brokerService.GetBroker(Id);
            if (broker == null)
            {
                TempData["Error"] = "Nie ma takiego komisanta";
                return RedirectToAction("Brokers");
            }

            BrokerViewModel viewModel = Mapper.Map<BrokerViewModel>(broker);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBroker(BrokerViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            Broker broker = _brokerService.GetBroker(viewModel.ID);

            if(broker == null)
            {
                TempData["Error"] = "Nie ma takiego komisanta";
                return RedirectToAction("Brokers");
            }

            if (_brokerService.CheckIfContractNumberisTaken(viewModel.Contract))
            {
                if (broker.ID != viewModel.ID)
                {
                    ModelState.AddModelError("", "Ten numer umowy jest już zajety");
                    return View(viewModel);
                }
            }


            Mapper.Map(viewModel, broker);

            bool result = _brokerService.EditBroker(broker);

            if (!result)
            {
                ModelState.AddModelError("", "Wystąpił błąd przy edytowaniu komisanta - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Dane Komisanta zstały zdytowane pomyślnie";
            return RedirectToAction("Brokers");
        }

        [HttpGet]
        public ActionResult ConfirmDeleteBroker(int Id)
        {
            Broker broker = _brokerService.GetBroker(Id);
            if (broker == null)
            {
                TempData["Error"] = "Nie ma takiego komisanta";
                return RedirectToAction("Brokers");
            }

            BrokerViewModel viewModel = Mapper.Map<BrokerViewModel>(broker);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDeleteBroker(BrokerViewModel viewModel)
        {
            Broker broker = _brokerService.GetBroker(viewModel.ID);

            if (broker == null)
            {
                TempData["Error"] = "Nie ma takiego komisanta";
                return RedirectToAction("Brokers");
            }

            bool result = _brokerService.DeleteBroker(broker);

            if (!result)
            {
                ModelState.Clear();
                ModelState.AddModelError("", "Wystąpił błąd przy usuwaniu komisanta - sprawdż logi");
                return View(viewModel);
            }
            TempData["message"] = "Komisant został usuniety pomyślnie";
            return RedirectToAction("Brokers");
        }

       
    }
}