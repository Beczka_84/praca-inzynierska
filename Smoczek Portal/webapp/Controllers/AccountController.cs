﻿#region Using

using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CloudCentrePortal.Models;
using CloudCentre.Services.Abstract;
using Hangfire;
using System.Net;
using AutoMapper;
using CloudCentre.DAL.Models;
using CloudCentre.Infrastructure.Email;
using System.Data.Entity.Infrastructure;
using CloudCentre.Controllers;
using CloudCentre.Filters;

#endregion

namespace CloudCentrePortal.Controllers
{
    [RolesActionFilter]
    public class AccountController : BaseController
    {
        private readonly IUserManagerService _userService;
        private readonly IBackgroundJobClient _backgroundJobClient;

        public AccountController(IUserManagerService userService, IBackgroundJobClient backgroundJobClient)
        {
            _userService = userService;
            _backgroundJobClient = backgroundJobClient;
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword(string userId)
        {
            return View(new AccountForgotPasswordModel());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(AccountForgotPasswordModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }
            var user = await _userService.GetUserByEmailAsync(viewModel.Email);
            if (user == null)
            {
                ModelState.AddModelError("", "Nie ma takiego użytkownika");
                return View(viewModel);
            }

            var token = await _userService.GeneratePasswordResetToken(user.Id);
            await _userService.SendResetPasswordMail(Url.Action("ConfirmPasswordResset", "Account", new { passToken = token, userId = user.Id }, protocol: Request.Url.Scheme), user.Id);

            TempData["message"] = string.Format("Email resetujący hasło został wysłany");
            return View();
        }

        [AllowAnonymous]
        public ActionResult ConfirmPasswordResset(string passToken, string userId)
        {
            if (passToken == null || userId == null)
            {
                return View("Error");
            }
            ConfirmPasswordResetViewModel model = new ConfirmPasswordResetViewModel() { Token = passToken, Id = userId };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmPasswordResset(ConfirmPasswordResetViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }
            var user = await _userService.GetUserById(viewModel.Id);
            if(user == null)
            {
                ModelState.AddModelError("", "Nie ma takiego użytkownika w bazie");
                return View(viewModel);
            }

            IdentityResult result = await _userService.ResetUserPassword(viewModel.Id, viewModel.Token, viewModel.NewPassword);

            if (result.Succeeded) { return RedirectToAction("Login"); }

            result.Errors.ToList().ForEach(x => ModelState.AddModelError("", x));
            return View(viewModel);
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return View(new AccountLoginModel() { ReturnUrl = returnUrl });
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            var user = await _userService.GetUserAsync(_userService.GetUserName(viewModel.Email), viewModel.Password);
            if (user != null)
            {
                try
                {
                    if (!await _userService.IsEmailConfirmed(user.Id))
                    {
                        ModelState.AddModelError("", "Musisz mieć potwierdzony adres email aby sie zalogować");
                        return View();
                    }

                    Session[CloudCentre.DAL.Models.Constants.AppUser] = user;
                    await _userService.SignUserAsync(user, viewModel.RememberMe);
                    return RedirectToLocal(viewModel.ReturnUrl);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "user can't be signed");
                    return View(viewModel);
                }
            }
            ModelState.AddModelError("", "Niepoprawne hasło lub login");
            return View(viewModel);
        }


        public ActionResult Logout()
        {
            _userService.SignOutUser();
            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {
            return View(new AccountRegistrationModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(AccountRegistrationModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            };

            AppIdentityUser user = Mapper.Map<AppIdentityUser>(viewModel);
            try
            {
                IdentityResult result = await _userService.CreateUserAsync(user, viewModel.Password);

                if (!result.Succeeded)
                {
                    result.Errors.ToList().ForEach(x => ModelState.AddModelError("", x));
                    return View(viewModel);
                }

                IdentityRole role = _userService.GetRole(viewModel.RoleId);
                IdentityResult roleresult = await _userService.AddUserToRoleAsync(user, role.Name);

                if (!roleresult.Succeeded)
                {
                    result.Errors.ToList().ForEach(a => ModelState.AddModelError("", a.ToString()));
                    return View(viewModel);
                }
                string token = await _userService.GenerateEmailToken(user.Id);
                await _userService.SendConfirmationEMail(Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = token, action = "Login", controller = "Account" }, protocol: Request.Url.Scheme), user.Id);

                TempData["message"] = "Użytkoniwk został pomyślnie dodany - email potwierdzający został wysłany na wskazany email";
                return RedirectToAction("AppUsersList", "Settings");
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("", ex.InnerException.ToString());
                return View(viewModel);
            }
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code, string action, string controller)
        {
            if (userId == null || code == null || action == null || controller == null)
            {
                //TempData["Error"] = "Błąd podczas potwierdzania emaila użytkownika";
                return RedirectToLocal();     
            }

            IdentityResult result = await _userService.ConfirmEmail(userId, code);
            if (!result.Succeeded)
            {
                //TempData["Error"] = "Błąd podczas potwierdzania emaila użytkownika";
                return RedirectToAction(action, controller);
            }

            return RedirectToAction(action, controller);
        }

        [AllowAnonymous]
        public ActionResult Lock()
        {
            return View();
        }


        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            if (!returnUrl.IsNullOrWhiteSpace() && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Index", "Home");
        }



    }
}