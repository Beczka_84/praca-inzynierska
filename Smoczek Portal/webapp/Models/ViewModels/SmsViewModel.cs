﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Models.ViewModels
{
    public class SmsViewModel
    {
        [Display(Name = "Komisanci")]
        public int[] BrokersIds { get; set; }

        [Display(Name = "Wiadomość do wysłania")]
        public string Message { get; set; }
        public SelectList BrokerSelect { get; set; }
    }
}