﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.ViewModels
{
    public class StoreSettingsViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Pole nazwa sklepu jest wymagane")]
        [Display(Name = "Nazwa Sklepu")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Pole addres sklepu jest wymagane")]
        [Display(Name = "Adress Sklepu")]
        public string Address { get; set; }
    }
}