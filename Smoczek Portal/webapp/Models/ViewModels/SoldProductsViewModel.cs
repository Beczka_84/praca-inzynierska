﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class SoldProductsViewModel
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }


        public double SoldPrice { get; set; }

        //[DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime SoldDate { get; set; }
        public double TotalSellValue { get; set; }
    }
}