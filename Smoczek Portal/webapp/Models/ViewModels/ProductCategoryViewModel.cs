﻿using CloudCentre.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class ProductCategoryViewModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name="Nazwa Kategorii")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Numeracja początkowa")]
        [Range(1, int.MaxValue , ErrorMessage = "Numer musi byc wiekszy niz 0")]
        public int StartNumber { get; set; }


        [Required]
        [Display(Name = "Numeracja koncowa")]
        [Range(1, int.MaxValue, ErrorMessage = "Numer musi byc wiekszy niz 0")]
        public int EndNumber { get; set; }

    }
}