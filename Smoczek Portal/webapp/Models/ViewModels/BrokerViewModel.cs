﻿using CloudCentre.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class BrokerViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Pole imie jest wymagane")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Pole nazwisko jest wymagane")]
        [Display(Name = "Nazwisko")]

        public string LastName { get; set; }

        [Required(ErrorMessage = "Pole dowód osobisty jest wymagane")]
        [Display(Name = "Dowód Osobisty")]

        public string Card { get; set; }

        [Required(ErrorMessage = "Pole adres jest wymagane")]
        [Display(Name = "Adres")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Pole numer umowy jest wymagane")]
        [Range(1, int.MaxValue, ErrorMessage = "Numer umowy musi byc wiekszy niz 0")]
        [Display(Name = "Numer Umowy")]
        public int Contract { get; set; }

        [EmailAddress]
        [Display(Name = "E-mail")]
        //[Required(ErrorMessage = "Pole jest wymagane")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber, ErrorMessage = "Pole musi być numeryczne")]
        [Required(ErrorMessage = "Pole jest wymagane")]
        [Display(Name = "Numer Telefonu")]
        public int? Phone { get; set; }


        public string FullName
        {
            get { return string.Join(" ", FirstName, LastName); }
          
        }

       

    }
}