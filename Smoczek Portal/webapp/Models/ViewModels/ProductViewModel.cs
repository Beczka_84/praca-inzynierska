﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class ProductViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Pole kategori produktu jest wymagane")]
        [Display(Name ="Nr Katalogowy")]
        public int CatalogNumber { get; set; }

        [Required(ErrorMessage = "Pole ilośc jest wymagane")]
        [Display(Name = "Ilość")]
        [Range(1, int.MaxValue, ErrorMessage = "Ilośc musi byc wiekszy niz 0")]
        public int Quantity { get; set; }

        [Required]
        [Display(Name = "Cena")]
        [Range(0.1, int.MaxValue, ErrorMessage = "Cena musi byc wiekszy niz 0")]
        public double OrgPrice { get; set; }

        [Display(Name = "Cena Po Przecenie")]
        public double DiscountPrice { get; set; }

  
        [Display(Name = "Data całkowitej sprzedaży produktu")]
        [DataType(DataType.Date)]
        public DateTime? SaleDate { get; set; }

        [Display(Name = "Uwagi")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }
        public int BrokerID { get; set; }
        public int ProductCategoryID { get; set; }


        [Display(Name = "Nazwa komisanta")]
        public string BrokerName { get; set; }

        [Display(Name = "Nazwa Kategori Produktu")]
        public string ProductCategoryName { get; set; }

        public DateTime TimeOfCreation { get; set; }
        public string FullContract { get; set; }

        public string BrokerContract { get; set; }

    }
}