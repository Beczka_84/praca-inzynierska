﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class IdentityUserEditViewModel
    {
        [Required]
        public string Id { get; set; }


        [Display(Name = "Nazwa Użytkownika")]
        public string UserName { get; set; }

        [Display(Name = "Imię Użytkownika")]
        public string FirstName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Nazwisko Użytkownika")]
        public string LastName { get; set; }
    }
}