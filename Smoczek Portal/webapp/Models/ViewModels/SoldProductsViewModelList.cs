﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class SoldProductsViewModelList
    {
        public List<SoldProductsViewModel> viewModel { get; set; }
        public int  brokerID { get; set; }
    }
}