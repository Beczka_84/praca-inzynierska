﻿using CloudCentre.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class ReportViewModel
    {
        public List<SoldProduct> SoldProducts { get; set; }
        public double TotalValue { get; set; }
        public int BrookerId { get; set; }
        public string FullName { get; set; }
        public string ConctractNumber { get; set; }

        public string ChartData { get; set; }

        public ReportViewModel()
        {
            SoldProducts = new List<SoldProduct>();
        }
    }
}