﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class ProductViewModelList
    {
        public List<ProductViewModel> ViewModel { get; set; }
        public int BrokerId { get; set; }
        public string BrokerName { get; set; }
        public int Contract { get; set; }
    }
}