﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.DAL.ViewModels
{
    public class AppIdentityUserViewModel
    {
        [Required]
        public string Id { get; set; }

        public string Role { get; set; }

        [Display(Name="Rola Użytkownika")]

        public string RoleId { get; set; }

        [Display(Name = "Nazwa Użytkownika")]
        public string UserName { get; set; }

        [Display(Name = "Imię Użytkownika")]
        public string FirstName { get; set; }

        public string Email { get; set; }

        [Display(Name = "Nazwisko Użytkownika")]
        public string LastName { get; set; }
    }
}