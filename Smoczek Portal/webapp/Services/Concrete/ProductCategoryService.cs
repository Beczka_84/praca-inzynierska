﻿using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CloudCentre.DAL.Models;
using CloudCentre.DAL;
using NLog;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Data.SqlClient;

namespace CloudCentre.Services.Concrete
{
    public class ProductCategoryService : IProductCategoryService
    {

        private readonly AppContext _db;
        private readonly ILogger _logger;

        public ProductCategoryService(AppContext db, ILogger logger)
        {
            this._db = db;
            this._logger = logger;
        }

        public bool CheckIfProductCategoryisTaken(string Name)
        {
            return _db.ProductCategorys.Any(x => x.Name == Name);
        }

        public bool CreateProductCategory(ProductCategory productCategory)
        {
            try
            {
                _db.ProductCategorys.Add(productCategory);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "CreateProductCategory", "ProductCategoryService"));
                return false;
                throw;
            }
        }

        public bool DeleteProductCategory(ProductCategory productCategory)
        {
            try
            {
                _db.ProductCategorys.Remove(productCategory);
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "DeleteProductCategory", "ProductCategoryService", sqlException.Message));
                }
                return false;
                throw;
            }


            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "DeleteProductCategory", "ProductCategoryService"));
                return false;
                throw;
            }
        }

        public bool EditProductCategory(ProductCategory productCategory)
        {
            try
            {
                _db.ProductCategorys.Attach(productCategory);
                _db.Entry(productCategory).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateConcurrencyException e)
            {
                _logger.Error(e, string.Format("Concurrency Exception in method {0}, service {1}", "EditProductCategory", "ProductCategoryService"));
                return false;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "EditProductCategory", "ProductCategoryService", sqlException.Message));
                }
                return false;
                throw;
            }

            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "EditProductCategory", "ProductCategoryService"));
                return false;
                throw;
            }
        }

        public IQueryable<ProductCategory> GetProductCategorys()
        {
            try
            {
                return _db.ProductCategorys;
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetProductCategory", "ProductCategoryService"));
                throw;
            }
        }

        public ProductCategory GetProductCategory(int id)
        {
            try
            {
                return _db.ProductCategorys.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetProductCategory", "ProductCategoryService"));
                throw;
            }
        }
    }
}