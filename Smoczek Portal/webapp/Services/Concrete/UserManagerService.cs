﻿using CloudCentre.App_Start;
using CloudCentre.DAL.Models;
using CloudCentre.Infrastructure.Email;
using CloudCentre.Services.Abstract;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CloudCentre.Services.Concrete
{
    public class UserManagerService: IUserManagerService
    {
        private readonly UserManager<AppIdentityUser> _userMenager;
        private readonly SignInManager<AppIdentityUser, string> _signInMenager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly ILogger _logger;

        public UserManagerService(UserManager<AppIdentityUser> userMenager, IAuthenticationManager authenticationManager, RoleManager<IdentityRole> roleManager, ILogger logger)
        {
            this._userMenager = userMenager;
            this._authenticationManager = authenticationManager;
            this._roleManager = roleManager;
            this._logger = logger;

            _userMenager.UserValidator = new UserValidator<AppIdentityUser>(userMenager) { RequireUniqueEmail = true, AllowOnlyAlphanumericUserNames = false };
            _userMenager.PasswordValidator = new PasswordValidator() { RequiredLength = 6, RequireLowercase = true, RequireUppercase = true, RequireDigit = true };
            _signInMenager = new SignInManager<AppIdentityUser, string>(_userMenager, _authenticationManager);

            _userMenager.EmailService = new EmailService();

            var dataProtectionProvider = Startup.dataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                IDataProtector dataProtector = dataProtectionProvider.Create("ASP.NET Identity");
                userMenager.UserTokenProvider = new DataProtectorTokenProvider<AppIdentityUser>(dataProtector);
            }
        }


        #region AppUserHelper
        public Task<AppIdentityUser> GetUserAsync(string name, string password)
        {
            return _userMenager.FindAsync(name, password);
        }

       
        public Task<AppIdentityUser> GetUserById(string id)
        {

            return _userMenager.FindByIdAsync(id);
        }
        public AppIdentityUser GetUserByIdSync(string id)
        {

            return _userMenager.FindById(id);
        }

        public Task<AppIdentityUser> GetUserByEmailAsync(string email)
        {
            return _userMenager.FindByEmailAsync(email);
        }
      

        public string GetUserName(string email)
        {
            AppIdentityUser user = _userMenager.FindByEmail(email);
            if (user != null) { return user.UserName; }
            return string.Empty;
        }

      

        #endregion

        #region AppSign
        public Task SignUserAsync(AppIdentityUser user, bool isPersistant)
        {
            return _signInMenager.SignInAsync(user, isPersistant, false);
        }

        public void SignOutUser()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public async Task SendConfirmationEMail(string callbackUrl, string Id)
        {
            await _userMenager.SendEmailAsync(Id, "Potwierdż swoje konto", "Proszę potwierdz swoje konto klikając w poniższy link: \"" + callbackUrl + "\"");
        }

        public async Task<bool> IsEmailConfirmed(string Id)
        {
            var result = await _userMenager.IsEmailConfirmedAsync(Id);
            return result;
        }

        public async Task<string> GenerateEmailToken(string id)
        {
            return await _userMenager.GenerateEmailConfirmationTokenAsync(id);
        }

        public async Task<IdentityResult> ConfirmEmail(string id, string token)
        {
            return await _userMenager.ConfirmEmailAsync(id, token);
        }
        #endregion

        #region AppUsers
        public async Task<IdentityResult> CreateUserAsync(AppIdentityUser user, string password)
        {
            IdentityResult result = await _userMenager.CreateAsync(user, password);
            CheckIfError(result);
            return result;
        }
        public async Task<IdentityResult> UpdateUserData(AppIdentityUser user)
        {
            IdentityResult result = await _userMenager.UpdateAsync(user);
            CheckIfError(result);
            return result;
        }
        public async Task<IdentityResult> RemoveUserData(AppIdentityUser user)
        {
            IdentityResult result = await _userMenager.DeleteAsync(user);
            CheckIfError(result);
            return result;

        }

      

        public async Task<ClaimsIdentity> CreateUserIdentityAsync(AppIdentityUser user, string type)
        {
            var userIdentity = await _userMenager.CreateIdentityAsync(user, type);
            return userIdentity;
        }

        public IList<AppIdentityUser> GetAllUsers()
        {
            return _userMenager.Users.ToList();
        }
        #endregion

        #region AppUsersRoles
        public Task<IdentityResult> AddUserToRoleAsync(AppIdentityUser user, string role)
        {
            return _userMenager.AddToRoleAsync(user.Id, role);
        }

        public Task<IdentityResult> RemoveUserFromRoleAsync(AppIdentityUser user, string role)
        {
            return _userMenager.RemoveFromRoleAsync(user.Id, role);
        }

        public IList<string> GetUserRole(string id)
        {
            return _userMenager.GetRoles(id);
           
        }

        public IdentityRole GetRole(string id)
        {
            return _roleManager.FindById(id);
        }

        public IQueryable<IdentityRole> GetAllRoles()
        {
            return _roleManager.Roles;

        }
        #endregion

        #region Password
        public async Task<string> GeneratePasswordResetToken(string id)
        {
            var token = await _userMenager.GeneratePasswordResetTokenAsync(id);
            return token;
        }

        public async Task<IdentityResult> ResetUserPassword(string id, string token, string newPassword)
        {
            return await _userMenager.ResetPasswordAsync(id, token, newPassword);
        }

        public async Task SendResetPasswordMail(string callbackUrl, string Id)
        {
            await _userMenager.SendEmailAsync(Id, "Reset hasła", "Zresetujesz hasło klikając na ten link: " + callbackUrl + " ");
        }


        public async Task SendNotyficationMail(string Id)
        {
            await _userMenager.SendEmailAsync(Id, "Powiadomienie", "Zostałeś dodany do strony");
        }

        #endregion

        private void CheckIfError(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                StringBuilder builder = new StringBuilder();
                result.Errors.ToList().ForEach(x => builder.Append(x));
                _logger.Error(builder);
            }
        }
    }
}
