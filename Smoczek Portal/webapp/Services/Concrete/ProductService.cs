﻿using System;
using System.Linq;
using CloudCentre.DAL;
using CloudCentre.DAL.Models;
using CloudCentre.Services.Abstract;
using NLog;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Data.SqlClient;

namespace CloudCentre.Services.Concrete
{
    public class ProductService : IProductService
    {
        private readonly AppContext _db;
        private readonly ILogger _logger;

        public ProductService(AppContext db, ILogger logger)
        {
            this._db = db;
            this._logger = logger;
        }

        public bool CheckIfProductNumberisTaken(int brokerId , int catalogNumber)
        {
            return _db.Products.Any(x => x.BrokerID == brokerId && x.CatalogNumber == catalogNumber);
        }

        public bool CreateProduct(Product product)
        {
            try
            {
                _db.Products.Add(product);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "CreateProduct", "ProductService"));
                return false;
                throw;
            }
        }

        public bool DeleteProduct(Product product)
        {
            try
            {
                _db.Products.Remove(product);
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "DeleteProduct", "ProductService", sqlException.Message));
                }
                return false;
                throw;
            }

            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "DeleteProduct", "ProductService"));
                return false;
                throw;
            }
        }

        public bool EditProduct(Product product)
        {
            try
            {
                _db.Products.Attach(product);
                _db.Entry(product).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateConcurrencyException e)
            {
                _logger.Error(e, string.Format("Concurrency Exception in method {0}, service {1}", "EditProduct", "ProductService"));
                return false;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "EditProduct", "ProductService", sqlException.Message));
                }
                return false;
                throw;
            }

            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "EditProduct", "ProductService"));
                return false;
                throw;
            }
        }

        public Product GetProduct(int id)
        {
            try
            {
                return _db.Products.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetProduct", "ProductService"));
                throw;
            }
        }

        public IQueryable<Product> GetProducts()
        {
            try
            {
                return _db.Products;
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetProduct", "ProductService"));
                throw;
            }
        }

        public IQueryable<Product> GetProducts(int brokerId)
        {
            try
            {
                return _db.Products.Where(x=>x.BrokerID == brokerId);
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetProduct", "ProductService"));
                throw;
            }
        }

        public SoldProduct GetSoldProduct(int id)
        {
            return _db.SoldProducts.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable<SoldProduct> GetSoldProducts(int id)
        {
            return _db.SoldProducts.Where(x => x.ProductID == id);
        }

        public IQueryable<SoldProduct> GetSoldProducts()
        {
            return _db.SoldProducts;
        }

        public bool RemoveSoldProduct(SoldProduct soldProduct)
        {
            try
            {
                _db.SoldProducts.Remove(soldProduct);
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "RemoveSoldProduct", "ProductService", sqlException.Message));
                }
                return false;
                throw;
            }

            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "RemoveSoldProduct", "ProductService"));
                return false;
                throw;
            }
        }

        public int RetriveLastProductNumber(int brokerId, int productCategoryId)
        {
            if (_db.Products.Any(x => x.BrokerID == brokerId && x.ProductCategoryID == productCategoryId))
            {
                return _db.Products.Where(x=>x.BrokerID == brokerId && x.ProductCategoryID == productCategoryId).Max(x => x.CatalogNumber) + 1;
            }

            return _db.ProductCategorys.FirstOrDefault(x=>x.ID == productCategoryId)?.StartNumber ?? 1;
        }

        public bool SaleProduct(SoldProduct soldProduct)
        {
            try
            {
                _db.SoldProducts.Add(soldProduct);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "SaleProduct", "ProductService"));
                return false;
                throw;
            }
        }

        public bool EditSaleProduct(SoldProduct saleProduct)
        {
            try
            {
                _db.SoldProducts.Attach(saleProduct);
                _db.Entry(saleProduct).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateConcurrencyException e)
            {
                _logger.Error(e, string.Format("Concurrency Exception in method {0}, service {1}", "EditSaleProduct", "ProductService"));
                return false;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "EditSaleProduct", "ProductService", sqlException.Message));
                }
                return false;
                throw;
            }

            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "EditSaleProduct", "ProductService"));
                return false;
                throw;
            }
        }
    }
}