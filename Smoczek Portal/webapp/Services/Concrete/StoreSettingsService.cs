﻿using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CloudCentre.DAL.Models;
using CloudCentre.DAL;
using System.Data.Entity;
using NLog;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace CloudCentre.Services.Concrete
{
    public class StoreSettingsService : IStoreSettingsService
    {
        private readonly AppContext _db;
        private readonly ILogger _logger;
        public StoreSettingsService(AppContext db, ILogger logger)
        {
            this._db = db;
            this._logger = logger;
        }

        public bool CreateStoreSettings(StoreSettings storeSettings)
        {
            try
            {
                _db.StoreSettings.Add(storeSettings);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "CreateStoreSettings", "StoreSettingsService"));
                return false;
                throw;
            }
           
        }

        public bool DeleteStoreSettings(StoreSettings storeSettings)
        {
            try
            {
                _db.StoreSettings.Remove(storeSettings);
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "DeleteStoreSettings", "StoreSettingsService", sqlException.Message));
                }
                return false;
                throw;
            }

            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "DeleteStoreSettings", "StoreSettingsService"));
                return false;
                throw;
            }
        }

        public bool EditStoreSettings(StoreSettings storeSettings)
        {
            try
            {
                _db.StoreSettings.Attach(storeSettings);
                _db.Entry(storeSettings).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateConcurrencyException e)
            {
                _logger.Error(e,string.Format("Concurrency Exception in method {0}, service {1}", "EditStoreSettings", "StoreSettingsService"));
                return false;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "EditStoreSettings", "StoreSettingsService", sqlException.Message));
                }
                return false;
                throw;
            }


            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "EditStoreSettings", "StoreSettingsService"));
                return false;
                throw;
            }
        }

        public StoreSettings GetStoreSettings(int id)
        {
            try
            {
                return _db.StoreSettings.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetStoreSettings", "StoreSettingsService"));
                throw;
            }
          
        }

        public StoreSettings GetStoreSettings()
        {
            try
            {
                return _db.StoreSettings.FirstOrDefault();
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetStoreSettings", "StoreSettingsService"));
                throw;
            }
          
        }
    }
}