﻿using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CloudCentre.DAL.Models;
using CloudCentre.DAL;
using NLog;

using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Data.SqlClient;

namespace CloudCentre.Services.Concrete
{
    public class BrokerService : IBrokerService
    {
        private readonly AppContext _db;
        private readonly ILogger _logger;

        public BrokerService(AppContext db, ILogger logger)
        {
            this._db = db;
            this._logger = logger;
        }

        public bool CreateBroker(Broker broker)
        {
            try
            {
                broker.TimeOfCreation = DateTime.UtcNow;
                _db.Brokers.Add(broker);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "CreateBroker", "BrokerService"));
                return false;
                throw;
            }
        }

        public bool DeleteBroker(Broker broker)
        {
            try
            {
                _db.Brokers.Remove(broker);
                _db.SaveChanges();
                return true;
            }
            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}" , "DeleteBroker", "BrokerService", sqlException.Message));
                }
                return false;
                throw;
            }

            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "DeleteBroker", "BrokerService"));
                return false;
                throw;
            }
        }

        public bool EditBroker(Broker broker)
        {
            try
            {
                _db.Brokers.Attach(broker);
                _db.Entry(broker).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }

            catch (DbUpdateConcurrencyException e)
            {
                _logger.Error(e, string.Format("Concurrency Exception in method {0}, service {1}", "EditBroker", "BrokerService"));
                return false;
            }

            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null)
                {
                    SqlException sqlException = e.InnerException.InnerException as SqlException;
                    _logger.Error(e, string.Format("Error in method {0}, service {1} Exception {2}", "EditBroker", "BrokerService", sqlException.Message));
                }
                return false;
                throw;
            }

            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "EditBroker", "BrokerService"));
                return false;
                throw;
            }
        }

        public Broker GetBroker(int id)
        {
            try
            {
                return _db.Brokers.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetBroker", "BrokerService"));
                throw;
            }
        }

        public IQueryable<Broker> GetBrokers()
        {
            try
            {
                return _db.Brokers;
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("Error in method {0}, service {1}", "GetBrokers", "BrokerService"));
                throw;
            }
        }

        public bool CheckIfContractNumberisTaken(int Contract)
        {
            return _db.Brokers.Any(x => x.Contract == Contract);
        }

        public int RetriveLastContranctNumber()
        {
            if (_db.Brokers.Any())
            {
                return _db.Brokers.Max(x => x.Contract) + 1;
            }
            return 1;
        }
    }
}