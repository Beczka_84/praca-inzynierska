﻿using CloudCentre.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IProductCategoryService
    {
        ProductCategory GetProductCategory(int id);
        IQueryable<ProductCategory> GetProductCategorys();
        bool CreateProductCategory(ProductCategory productCategory);
        bool EditProductCategory(ProductCategory productCategory);
        bool DeleteProductCategory(ProductCategory productCategory);
        bool CheckIfProductCategoryisTaken(string Name);
    }
}
