﻿using CloudCentre.DAL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IUserManagerService
    {
        #region AppUserHelper
        Task<AppIdentityUser> GetUserAsync(string name, string password);

        Task<AppIdentityUser> GetUserById(string id);
        AppIdentityUser GetUserByIdSync(string id);
        Task<AppIdentityUser> GetUserByEmailAsync(string email);
        string GetUserName(string email);
        #endregion

        #region AppSign
        Task SignUserAsync(AppIdentityUser user, bool isPersistant);
        void SignOutUser();
        Task SendConfirmationEMail(string callbackUrl, string Id);
        Task<bool> IsEmailConfirmed(string Id);
        Task<string> GenerateEmailToken(string id);
        Task<IdentityResult> ConfirmEmail(string id, string token);
        #endregion

        #region AppUsers
        Task<IdentityResult> CreateUserAsync(AppIdentityUser user, string password);
        Task<IdentityResult> RemoveUserData(AppIdentityUser user);
        Task<IdentityResult> UpdateUserData(AppIdentityUser user);
        Task<ClaimsIdentity> CreateUserIdentityAsync(AppIdentityUser user, string type);
        IList<AppIdentityUser> GetAllUsers();
        #endregion

        #region AppUsersRoles
        Task<IdentityResult> AddUserToRoleAsync(AppIdentityUser user, string role);
        Task<IdentityResult> RemoveUserFromRoleAsync(AppIdentityUser user, string role);

        IList<string> GetUserRole(string id);
        IdentityRole GetRole(string id);
        IQueryable<IdentityRole> GetAllRoles();
        #endregion

        #region Password
        Task<string> GeneratePasswordResetToken(string id);
        Task<IdentityResult> ResetUserPassword(string id, string token, string newPassword);
        Task SendResetPasswordMail(string callbackUrl, string Id);
        Task SendNotyficationMail(string Id);
        #endregion

     
    }
}
