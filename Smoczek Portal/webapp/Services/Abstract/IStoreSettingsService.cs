﻿using CloudCentre.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IStoreSettingsService
    {
        StoreSettings GetStoreSettings(int id);
        StoreSettings GetStoreSettings();
        bool CreateStoreSettings(StoreSettings storeSettings);
        bool EditStoreSettings(StoreSettings storeSettings);
        bool DeleteStoreSettings(StoreSettings storeSettings);

    }
}
