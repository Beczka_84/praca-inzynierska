﻿using CloudCentre.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IProductService
    {
        Product GetProduct(int id);
        IQueryable<Product> GetProducts();
        IQueryable<Product> GetProducts(int brokerId);
        bool CreateProduct(Product product);
        bool EditProduct(Product product);
        bool EditSaleProduct(SoldProduct saleProduct);
        bool DeleteProduct(Product product);
        bool SaleProduct(SoldProduct soldProduct);
        bool RemoveSoldProduct(SoldProduct soldProduct);
        SoldProduct GetSoldProduct(int id);
        IQueryable<SoldProduct> GetSoldProducts(int id);
        IQueryable<SoldProduct> GetSoldProducts();
        int RetriveLastProductNumber(int brokerId, int productCategoryId);
        bool CheckIfProductNumberisTaken(int brokerId, int catalogNumber);
    }
}
