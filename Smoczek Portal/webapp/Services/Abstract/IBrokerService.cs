﻿using CloudCentre.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IBrokerService
    {
        Broker GetBroker(int id);
        IQueryable<Broker> GetBrokers();
        bool CreateBroker(Broker broker);
        bool EditBroker(Broker broker);
        bool DeleteBroker(Broker broker);
        bool CheckIfContractNumberisTaken(int Contract);
        int RetriveLastContranctNumber();
    }
}
