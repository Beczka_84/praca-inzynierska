﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Infrastructure.Email
{
    [AutomaticRetry(Attempts = 3)]
    public class Email : Postal.Email
    {
        public string To { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }

        public void Send(string To, string Title, string Body)
        {
            this.Body = Body;
            this.To = To;
            this.Title = Title;
            base.Send();
        }
    }
}