﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace CloudCentre.Infrastructure.Email
{
    /// <summary>
    /// Email service used to genereate email for identity 2.0 you could change this to Postal for example.
    /// </summary>
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {

            var mailMessage = new MailMessage(
            "lukasz.ozog@outlook.com",
            message.Destination,
            message.Subject,
            message.Body
            );

            SmtpClient client = new SmtpClient("smtp-mail.outlook.com", 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential("lukasz.ozog@outlook.com", "q1w2e3r4t5y6"),
                EnableSsl = true
            };
            return client.SendMailAsync(mailMessage);
        }
    }
}