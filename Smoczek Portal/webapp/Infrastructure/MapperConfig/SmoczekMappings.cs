﻿using AutoMapper;
using CloudCentre.DAL.Models;
using CloudCentre.DAL.ViewModels;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Infrastructure.MapperConfig
{
    public class SmoczekMappings : Profile
    {
        protected override void Configure()
        {

            Mapper.CreateMap<SoldProduct, SoldProductsViewModel>()
                  .ForMember(dest => dest.TotalSellValue, opts => opts.ResolveUsing<TotalValueResolver>());
            
            Mapper.CreateMap<SoldProductsViewModel, SoldProduct>();

            Mapper.CreateMap<Product, SoldProduct>()
                .ForMember(dest => dest.ProductID, opts => opts.MapFrom(y => y.ID))
                .ForMember(dest => dest.ID, opts => opts.Ignore())
                .ForMember(dest => dest.Quantity, opts => opts.UseValue(1))
                .ForMember(dest => dest.SoldDate, opts => opts.UseValue(DateTime.Now));



            Mapper.CreateMap<ProductViewModel, Product>()
                .ForMember(dest => dest.ID, opts => opts.Ignore());
            Mapper.CreateMap<Product, ProductViewModel>()
                .ForMember(dest => dest.ProductCategoryName, opts => opts.MapFrom(x => x.ProductCategory.Name))
                .ForMember(dest => dest.BrokerName, opts => opts.ResolveUsing<BrokerFullNameResolver>())
                .ForMember(dest => dest.ProductCategoryName, opts => opts.MapFrom(x => x.ProductCategory.Name))
                .ForMember(dest => dest.FullContract, opts => opts.MapFrom(x => string.Join("", x.Broker.Contract, "/", x.Broker.TimeOfCreation.Value.Year.ToString())));

            Mapper.CreateMap<ProductCategoryViewModel, ProductCategory>()
               .ForMember(dest => dest.ID, opts => opts.Ignore());
            Mapper.CreateMap<ProductCategory, ProductCategoryViewModel>();

            Mapper.CreateMap<BrokerViewModel, Broker>()
               .ForMember(dest => dest.ID, opts => opts.Ignore());
            Mapper.CreateMap<Broker, BrokerViewModel>();

            Mapper.CreateMap<AccountRegistrationModel, AppIdentityUser>();
            Mapper.CreateMap<AppIdentityUser, AccountRegistrationModel>();

            Mapper.CreateMap<IdentityUserEditViewModel, AppIdentityUser>();
            Mapper.CreateMap<AppIdentityUser, IdentityUserEditViewModel>();

            Mapper.CreateMap<StoreSettingsViewModel, StoreSettings>();
            Mapper.CreateMap<StoreSettings, StoreSettingsViewModel>();

            Mapper.CreateMap<AppIdentityUserViewModel, AppIdentityUser>()
                 .ForMember(dest => dest.Id, opts => opts.Ignore());

            Mapper.CreateMap<AppIdentityUser, AppIdentityUserViewModel>()
                  .ForMember(dest => dest.Role, opts => opts.ResolveUsing<RoleResolver>())
                  .ForMember(dest => dest.RoleId, opts => opts.MapFrom(x=>x.Roles.FirstOrDefault().RoleId));
        }


        public class TotalValueResolver : ValueResolver<SoldProduct, double>
        {
            protected override double ResolveCore(SoldProduct source)
            {

                return source.Quantity * source.SoldPrice;
            }
        }

        public class BrokerFullNameResolver : ValueResolver<Product, string>
        {
            protected override string ResolveCore(Product source)
            {

                return string.Join(" ", source.Broker.FirstName, source.Broker.LastName);
            }
        }

        public class RoleResolver : ValueResolver<AppIdentityUser, string>
        {
            private readonly IUserManagerService _userManagerService;

            public RoleResolver(IUserManagerService userManagerService)
            {
                this._userManagerService = userManagerService;
            }

            protected override string ResolveCore(AppIdentityUser source)
            {
                string role = _userManagerService.GetUserRole(source.Id).FirstOrDefault();
                return role ?? string.Empty;
            }
        }
    }
}