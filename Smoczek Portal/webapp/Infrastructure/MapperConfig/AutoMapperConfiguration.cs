﻿using AutoMapper;
using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Infrastructure.MapperConfig
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<SmoczekMappings>();
               
            });
            IKernel kernel = new Bootstrapper().Kernel;
            Mapper.Configuration.ConstructServicesUsing(UserManagerService => kernel.Get(UserManagerService));
        }
    }
}