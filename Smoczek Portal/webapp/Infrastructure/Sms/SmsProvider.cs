﻿using Hangfire;
using Plivo.API;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace CloudCentre.Infrastructure.Sms
{
    [AutomaticRetry(Attempts = 3)]
    public class SmsProvider
    {
        public string To { get; set; }

        public string Txt { get; set; }


        public void Send(string to, string txt)
        {
            string auth = System.Configuration.ConfigurationManager.AppSettings["auth"].ToString();
            string token = System.Configuration.ConfigurationManager.AppSettings["token"].ToString();
            string from = System.Configuration.ConfigurationManager.AppSettings["numberFrom"].ToString();


            this.To = to;
            this.Txt = txt;

            RestAPI plivo = new RestAPI(auth, token);
            IRestResponse<MessageResponse> resp = plivo.send_message(new Dictionary<string, string>()
            {
                { "src", from }, // Sender's phone number with country code
                { "dst", string.Join("","48", to) }, // Receiver's phone number wiht country code
                { "text", txt }, // Your SMS text message
                { "method", "POST"} // Method to invoke the url
            });

            var result = resp;
        }
    }
}