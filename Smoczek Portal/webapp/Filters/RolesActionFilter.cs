﻿using CloudCentre.DAL.Models;
using CloudCentre.Services.Abstract;
using Ninject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace CloudCentre.Filters
{
    public class RolesActionFilter : ActionFilterAttribute
    {
        [Inject]
        public IUserManagerService _userManagerService { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if (HttpContext.Current.Cache[Constants.RolesViewBag] == null)
            //{
            //    HttpContext.Current.Cache.Insert(Constants.RolesViewBag, 
            //        _userManagerService.GetAllRoles().ToList(), 
            //        null, 
            //        Cache.NoAbsoluteExpiration, 
            //        Cache.NoSlidingExpiration, 
            //        CacheItemPriority.NotRemovable, 
            //        null);
            //}

            filterContext.Controller.ViewBag.AppRoles = new SelectList(_userManagerService.GetAllRoles().ToList(), "Id", "Name");
        }
    }
}