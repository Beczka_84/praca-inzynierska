﻿using CloudCentre.DAL.Models;
using Hangfire.Dashboard;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Filters
{
    public class RestrictiveAuthorizationFilter : IAuthorizationFilter
    {
        public bool Authorize(IDictionary<string, object> owinEnvironment)
        {
            var context = new OwinContext(owinEnvironment);
            if (context.Authentication.User.IsInRole(Roles.Admin)) { return true; }
            return false;
        }
    }
}