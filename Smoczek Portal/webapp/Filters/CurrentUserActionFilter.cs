﻿using CloudCentre.DAL.Models;
using CloudCentre.Services.Abstract;
using Microsoft.AspNet.Identity;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Filters
{
        public class CurrentUserActionFilter : ActionFilterAttribute
        {
            [Inject]
            public IUserManagerService _userManagerService { get; set; }

            public override void OnActionExecuted(ActionExecutedContext filterContext)
            {
                if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    if (filterContext.HttpContext.Session[DAL.Models.Constants.AppUser] == null)
                    {
                        filterContext.HttpContext.Session[DAL.Models.Constants.AppUser] = _userManagerService.GetUserByIdSync(filterContext.HttpContext.User.Identity.GetUserId());
                    }
                }
            }
        }
}