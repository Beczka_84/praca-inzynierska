﻿using AutoMapper;
using CloudCentre.DAL.Models;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services.Abstract;
using Ninject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace CloudCentre.Filters
{
    public class BrokerAttribute : FilterAttribute { }

    public class BrokerActionFilter : ActionFilterAttribute
    {
       
        private readonly IBrokerService _brokerService;

        public BrokerActionFilter(IBrokerService brokerService)
        {
            this._brokerService = brokerService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            List<BrokerViewModel> list = Mapper.Map<List<BrokerViewModel>>(_brokerService.GetBrokers());

            //if (HttpContext.Current.Cache[Constants.BrokerViewBag] == null)
            //{
            //    List<BrokerViewModel> list  = Mapper.Map<List<BrokerViewModel>>(_brokerService.GetBrokers());
            //    HttpContext.Current.Cache.Insert(Constants.BrokerViewBag,
            //       list,
            //        null,
            //        DateTime.Now.AddMinutes(1),
            //        Cache.NoSlidingExpiration,
            //        CacheItemPriority.NotRemovable,
            //        null);
            //}
            filterContext.Controller.ViewBag.BrokerViewBag = new SelectList((IEnumerable)list, "ID", "FullName");
            //filterContext.Controller.ViewBag.BrokerViewBag = new SelectList((IEnumerable)HttpContext.Current.Cache[Constants.BrokerViewBag], "ID", "FullName");
        }

    }
}