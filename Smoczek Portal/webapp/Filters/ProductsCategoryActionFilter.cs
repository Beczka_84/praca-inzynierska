﻿using CloudCentre.DAL.Models;
using CloudCentre.Services.Abstract;
using Ninject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace CloudCentre.Filters
{

    public class ProductsCategoryAttribute : FilterAttribute { }


    public class ProductsCategoryActionFilter  : ActionFilterAttribute
    {
      
        private readonly IProductCategoryService _productCategoryService;

        public ProductsCategoryActionFilter(IProductCategoryService productCategoryService)
        {
            this._productCategoryService = productCategoryService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if (HttpContext.Current.Cache[Constants.ProductCategoryViewBag] == null)
            //{
            //    var list = _productCategoryService.GetProductCategorys().ToList();
            //    HttpContext.Current.Cache.Insert(Constants.ProductCategoryViewBag,
            //       list,
            //        null,
            //        DateTime.Now.AddMinutes(1),
            //        Cache.NoSlidingExpiration,
            //        CacheItemPriority.NotRemovable,
            //        null);
            //}
            filterContext.Controller.ViewBag.ProductCategoryViewBag = new SelectList(_productCategoryService.GetProductCategorys(), "ID", "Name");
        }
    }
}