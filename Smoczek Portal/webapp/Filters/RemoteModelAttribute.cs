﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Filters
{
    public class RemoteModelAttribute : RemoteAttribute
    {
        public RemoteModelAttribute(string action, string controller) : base(action, controller)
        {
        }
    }
}